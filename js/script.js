// easy fetch util

let heatmapLayer = new HeatmapOverlay({
    radius: 40,
  	maxOpacity: .7,
    valueField: 'value',
    useLocalExtrema: true
})

let factoryIcon = L.icon({
    iconUrl: 'img/factory.png',
    iconSize: [20, 20]
})

let mapMarkers = L.layerGroup();
const map = L.map('map', {
    center: [-24.25, 133.416667],
    zoom: 4,
    minZoom: 4,
    layers: [
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png'),
        heatmapLayer
    ]
})

map.on('zoomend', () => {
    if (map.getZoom() < 9) {
        map.removeLayer(mapMarkers)
    } else {
        map.addLayer(mapMarkers)
    }
})

const s3fetch = name =>
	fetch("https://s3-ap-southeast-2.amazonaws.com/npi-govhack/" + name + ".json",
          { mode: 'cors' })
Promise.all([
        "anzsic_divisions", "facilities", "substances", "emissions0", "emissions1",
        "emissions2", "emissions3", "emissions4", "emissions5", "emissions6",
	].map(s3fetch)
)
.then(results => Promise.all(results.map(result => result.json())))
.then(jsonResults => {
    $('.active.page.dimmer').removeClass('active')
    
    const [anzsic_divisions, facilities, substances, ...emissionses] = jsonResults
    const emissions = Object.assign({}, ...emissionses)
	
    const emission_types = {
        air_point: "Controlled release of a gas into the atmosphere. Usually as a result of the manufacturing process.",
        air_fugitive: "Accidental release of a gas into the atmosphere.",
        water: "Regulated release of a chemical into nearby waterway.",
        land: "Accidental spill of chemical onto land, usually eventually reaching water-sources."
    }
    
    let filters = {
        anzsic_ids: [],
        substance_ids: [],
        emission_types: []
    }
    
	const $substanceDropdown = $('#substance-dropdown')
	const $industryDropdown = $('#industry-dropdown')
	const $emissionTypeDropdown = $('#emission-type-dropdown')

	//populate and init substance dropdown
	$('#substance-items').append(Object.keys(substances).reduce((html, id) =>
        html + 
            `<div class='item' data-value='${id}'>
                ${substances[id].name}
                <span class='right floated'
                        data-tooltip='More Info'
                        data-position='left center'>
                    <a href="${substances[id].url}" target="_blank">
                        <i class="info circle icon"></i>
                    </a>
                </span>
            </div>`, ""));

	
	$('#substance-clear-all').click(function() {
  		$('#substance-dropdown').dropdown('clear');
	});
    
	//industry dropdown
	$('#industry-items').append(Object.keys(anzsic_divisions).reduce((html, id) =>
        html + 
            `<div class='item' data-value='${id}'>
                ${anzsic_divisions[id].name}
            </div>`, ""));

	
	$('#industry-clear-all').click(function() {
  		$('#industry-dropdown').dropdown('clear');
	});
	
	//emission-type
	$('#emission-type-items').append(Object.keys(emission_types).reduce((html, id) =>
        html + 
            `<div class='item' data-value='${id}'>
                ${id}
                <span class='right floated emtype-tooltip'
                        data-tooltip='${emission_types[id]}'
                        data-position='left center'>
                        <i class="info circle icon"></i>
                </span>
            </div>`, ""));

	
	$('#emission-type-clear-all').click(function() {
  		$('#emission-type-dropdown').dropdown('clear');
	});
	
	$substanceDropdown
	  .dropdown({
		onChange: function() {
		  updateFilter('substance_ids','#substance-dropdown');
		}
	  });
	
	$industryDropdown
	  .dropdown({
		onChange: () => updateFilter('anzsic_ids','#industry-dropdown')
	  });
	
	$emissionTypeDropdown
	  .dropdown({
		onChange: () => updateFilter('emission_types','#emission-type-dropdown')
	  });

    let width = $('#timeline').css('width'),
        height = $('#timeline').css('height'),
        x = d3.scaleTime().range([0, width]),
        y = d3.scaleLinear().range([height, 0]),
        z = d3.scaleOrdinal(d3.schemeCategory10),
        stack = d3.stack(),
        xAxis = d3.axisBottom(x),
        yAxis = d3.axisLeft(y),
        svg = d3.select('#timeline')
    
    let brush = d3.brushX()
    	.extent([0, 0], [width, height])
    	.on('brush', () => {
            console.log('selected with brush: ', d3.event.selection)
        })
    
    let area = d3.area()
    	.curve(d3.curveMonotoneX)
    	.x(emissionTotal => emissionTotal.year)
    	.y0(height)
    	.y(emissionTotal => emissionTotal.amount)
    
    svg.append("defs").append("clipPath")
        .attr("id", "clip")
        .append("rect")
        .attr("width", width)
        .attr("height", height)
    
    // Do this every time a filter or brush selection changes
    const updateView = () => {
        let visible_emission_ids = filters.substance_ids
            .map(id => substances[id].emission_ids)
            .reduce((a, b) => a.concat(b), [])
        
        if (filters.anzsic_ids.length !== 0) {
            visible_emission_ids = visible_emission_ids
                .filter(id =>
                    filters.anzsic_ids.indexOf(
                        facilities[emissions[id].facility_id].anzsic_division)
                        >= 0
                )
        }
        
        const visible_emission_types = (() => {
        	if (filters.emission_types.length > 0) {
                return filters.emission_types
            } else {
                return Object.keys(emission_types)
            }
        })()
        
        let emissionTotals = {}
        
        visible_emission_ids.forEach(id => 
            emissionTotals[id] = visible_emission_types
                .map(type => emissions[id][type])
                .reduce((a, b) => a + b, 0))
        
        // update the heatmap
        let max_val = 0
        heatmapLayer.setData({
            data: visible_emission_ids
                .map(id => {
                    if (max_val < emissionTotals[id]) {
                        max_val = emissionTotals[id]
                    }
                    
                    return {
                        lat: emissions[id].latitude,
                        lng: emissions[id].longitude,
                        value: emissionTotals[id]
                    }
                }),
            max: max_val
        })
        
        // readd the facility icons
        mapMarkers.clearLayers()
        const facility_emissions = visible_emission_ids
        .reduce((facility_emissions, id) =>
                Object.assign({[emissions[id].facility_id]: {
                    location: [emissions[id].latitude, emissions[id].longitude],
                    [emissions[id].substance_id]: visible_emission_types
                    	.map(type => ({ [type]: emissions[id][type] }))
                    	.reduce((a, b) => Object.assign(a, b), {})
                }}, facility_emissions), {})
        Object.keys(facility_emissions)
        .forEach(id => {
            const location = facility_emissions[id].location
            delete facility_emissions[id].location
            
            //console.log(facility_emissions[id])
            
			let emissionsString = "";
			
			$.each(facility_emissions[id], ( substanceID, typedEmissions ) => {
				emissionsString += substances[substanceID].name  + "\n";
				console.log(facility_emissions[id]);
				(filters.emission_types && filters.emission_types.length) ?
				filters.emission_types.forEach(emissionType => {
					
					emissionsString += "  " + emissionType + ": " + typedEmissions[emissionType] + "\n"
				}) :
				Object.keys(emission_types).forEach(emissionType => {
					
					emissionsString += "  " + emissionType + ": " + typedEmissions[emissionType] + "\n"
				});
				emissionsString += "\n";
			});
			
			
            let marker =
                L.marker(location, {
                    icon: factoryIcon,
                    title: facilities[id].business_name + ":\n"
                        + facilities[id].facility_name + "\n\n"
					 + emissionsString
                })
            marker.on('click', () => {
                if (facilities[id].url !== "") {
                    window.open(facilities[id].url)
                }
            })
			

            mapMarkers.addLayer(marker)
        })
        	
        	
        
        let totals = filters.substance_ids.reduce(
        	(totals, id) => Object.assign({[id]: {}}, totals),
            {})
        
		var doughnut_totals = [];
		var doughnut_totals2 = [];
		var doughnut_totals3 = [];
		
        // update the timeline
        let min_year = 99999,
            max_year = -99999

        visible_emission_ids.forEach(id => {

            const emission = emissions[id]
            const emissionYear = new Date(
                	facilities[emission.facility_id]
            		.reports[emission.report_id].date)
            	.getFullYear()
            const emissionYearInt = parseInt(emissionYear)
            
            if (emissionYearInt < min_year) {
                min_year = emissionYearInt
            } else if (emissionYearInt > max_year) {
                max_year = emissionYearInt
            }
            
            // increment the totals for each year
            if (totals[emission.substance_id].hasOwnProperty(emissionYear)) {
                totals[emission.substance_id][emissionYear] += emissionTotals[id]
            } else {
                totals[emission.substance_id][emissionYear] = emissionTotals[id]
            }
			
			//For Doughnut chart
			let air_emission = emission.air_fugitive + emission.air_point
			let water_emission = emission.water
			let land_emission = emission.land
			
			let emissionCat = facilities[emission.facility_id].anzsic_division
			let emissionName = anzsic_divisions[emissionCat].name
			
			if (emissionCat in doughnut_totals){
				doughnut_totals[emissionName] += air_emission
				doughnut_totals2[emissionName] += water_emission
				doughnut_totals3[emissionName] += land_emission
			} else {
				doughnut_totals[emissionName] = air_emission 
				doughnut_totals2[emissionName] = water_emission 
				doughnut_totals3[emissionName] = land_emission
			}
        })
		
		doughnut(doughnut_totals, doughnut_totals2,  doughnut_totals3)
        

    }
	
    function updateFilter(filter, dropdownID)
	{
		let values = $(dropdownID).dropdown('get value');
		let valuesArr = (values != "") ? values.split(",") : [];
		filters[filter] = valuesArr;
		updateView();
	}
    updateView() // run it for the first time
    
})
