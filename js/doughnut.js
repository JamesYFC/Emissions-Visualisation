function getRandomColor(inputlen) {
	var letters = '0123456789ABCDEF';
	var colors = []
	for (j = 0; j < inputlen; j++){
		var color = '#';
		for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		colors.push(color);
	}

	return colors;
}

function getBorder(inputlen) {
	var borders = []
	
	for (i = 0; i < inputlen; i++){
		borders.push(0);
	}
	
	return borders;
}

function getLabel(input) {
	keys = Object.keys(input)
	
	return keys
}

function doughnut(inputs, inputs2, inputs3) {

	input = Object.values(inputs)
	input2 = Object.values(inputs2)
	input3 = Object.values(inputs3)

	$(document).ready(function(){
	colours = getRandomColor(input.length);
	var data = {
		labels: Object.keys(inputs),
		datasets: [
			{
				label: "Emission",
				data: input,
                backgroundColor: colours,
                borderColor: colours,
				borderWidth: getBorder(input.length)
			},
			{
				label: "Emission",
				data: input2,
                backgroundColor: colours,
                borderColor: colours,
				borderWidth: getBorder(input.length)
			},
			{
				label: "Emission",
				data: input3,
                backgroundColor: colours,
                borderColor: colours,
				borderWidth: getBorder(input.length)
			}
		]
    };
	
	var options = {
    responsive: true,
    title: {
        display: true,
        position: "top",
        text: "Doughnut Chart",
        fontSize: 18,
        fontColor: "#111"
    },
    legend: {
        display: true,
        position: "bottom",
        labels: {
            fontColor: "#333",
            fontSize: 16
        }
    }
	};
	
	var ctx = $("#mycanvas").get(0).getContext("2d");
    var myPieChart = new Chart(ctx,{
    type: 'doughnut',
    data: data,
	options: options
	});
		
	});
}

